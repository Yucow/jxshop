import request from './request'
import qs from 'qs'
import axios from 'axios'

axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v1/';
axios.interceptors.request.use(function (config) {
    config.headers['token'] = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

/**
 * 登录
 * @param {Object} params { username: '', password: ''}
 */
export function login (params) {
    return request({
        url: '/users/login',
        method: 'post',
        data: params
    })
}

/**
 * 获取当前用户的信息
 * @param {Object} params { userid, token }
 */
export function getuserinfo (params) {
    return request({
        url: '/users/getuserinfo',
        method: 'get',
        params
    })
}

export const postMsgApi = postData => {
    return axios.post('public/sendMsg.jsp', qs.stringify(postData)).then(res => res.data)
}
export const postLoginApi = postData => {
    return axios.post('public/login.jsp', qs.stringify(postData)).then(res => res.data)
}
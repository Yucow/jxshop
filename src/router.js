import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        // {
        //   path: '/',
        //   name: 'home',
        //   component: Home
        // },
        // {
        //     path: '/',
        //     name: 'about',
        //     // route level code-splitting
        //     // this generates a separate chunk (about.[hash].js) for this route
        //     // which is lazy-loaded when the route is visited.
        //     component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // }
        {
            path: '/userlogin',
            component: () => import('./views/login/UserLogin.vue')
        },
        {
            path: '/msglogin',
            component: () => import('./views/login/MsgLogin.vue')
        },
        {
            path: '/register',
            component: () => import('./views/login/Register.vue')
        },
    ]
})
router.beforeEach((to, from, next) => {
    if (to.path !== '/My') {
        next()
    } else {
        let token = localStorage.getItem('token')
        if (token) {
            next()
        } else {
            next('/userLogin')
        }
    }
})
export default router 

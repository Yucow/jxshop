import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 配置全局文件&样式
import '@/utils/rem.js'
import '@/utils/filters.js'
import '@/assets/css/reset.scss'

import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
